package com.nbu.breaker.brick.java;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.Timer;

import javax.swing.JPanel;

public class Gameplay extends JPanel implements KeyListener, ActionListener {

	public void actionPerformed(ActionEvent е) {
		// TODO Auto-generated method stub
		time.start();
		if (playing){

			if (new Rectangle(ballPosX, ballPosY, 20, 20).intersects(new Rectangle(playerPosX, 550, 100, 8))) {
				ballDirY = -ballDirY;

			}
		
		A: for(int i=0; i<mg.map.length; i++)
		{
			for (int j=0;j<mg.map[0].length; j++)
			{
				if(mg.map[i][j] > 0)
				{
					int brickX =j*mg.width + 80;
					int brickY = i*mg.height + 50;
					int brickWidth = mg.width;
					int brickHeight = mg.height;
					
					Rectangle rect = new Rectangle(brickX, brickY, brickWidth, brickHeight);
					Rectangle ballRect = new Rectangle(ballPosX, ballPosY,20,20);
					Rectangle brickRect = rect;
					
					if(ballRect.intersects(brickRect))
					{
						mg.setBrickValue(0, i, j);
						totalBricks--;
						score+=5;
						
						if(ballPosX +19 <= brickRect.x || ballPosX +1 >= brickRect.x +brickRect.width)
						{
							ballDirX = - ballDirX;
						}else
						{
							ballDirY =-ballDirY;
						}
						
						break A;
					}
				}
			}
		}
			
			
			
			ballPosX += ballDirX;
			ballPosY += ballDirY;

			if (ballPosX < 0) {
				ballDirX = -ballDirX;
			}
			if (ballPosY < 0) {
				ballDirY = -ballDirY;
			}
			if (ballPosX > 670) {
				ballDirX = -ballDirX;
			}
			repaint();

		}
	}

	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
			if (playerPosX >= 600) {
				playerPosX = 600;
			} else {
				move(false);
			}
		}
		if (e.getKeyCode() == KeyEvent.VK_LEFT) {
			if (playerPosX < 10) {
				playerPosX = 10;
			} else {
				move(true);
			}
		}
		if(e.getKeyCode() == KeyEvent.VK_ENTER)
		{
			if(!playing)
			{
				playing=true;
				ballPosX=120;
				ballPosY=350;
				ballDirX= -1;
				ballDirY =-2;
				totalBricks=21;
				mg = new MapGenerator(3,7);
			}
		}

	}

	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	private boolean playing = false;
	private int totalBricks =21;
	private int playerPosX = 310;
	private int playerPosY = 550;
	private int ballPosX = 120;
	private int ballPosY = 350;
	private int ballDirX = -1;
	private int ballDirY = -2;
	private int score = 0;
	private Timer time;
	private int delay = 8;
	public MapGenerator mg;

	public Gameplay() {
		mg = new MapGenerator(3, 7);
		addKeyListener(this);
		setFocusable(true);
		setFocusTraversalKeysEnabled(false);
		time = new Timer(delay, this);
		time.start();

	}

	public void move(boolean moveLeft) {
		playing = true;
		if (moveLeft) {
			playerPosX -= 20;
		} else {
			playerPosX += 20;
		}
	}

	public void paint(Graphics g) {
		// creating the background
		g.setColor(Color.black);
		g.fillRect(1, 1, 692, 592);

		mg.draw((Graphics2D) g);
		// creating the borders
		g.setColor(Color.yellow);
		g.fillRect(0, 0, 3, 592);
		g.fillRect(0, 0, 692, 3);
		g.fillRect(691, 0, 3, 592);
		//scores
		g.setColor(Color.YELLOW);
		g.setFont(new Font("serif",Font.BOLD, 25 ));
		g.drawString("" + score, 590, 30);
		// paddle
		g.setColor(Color.gray);
		g.fillRect(playerPosX, 550, 100, 8);

		// ball
		g.setColor(Color.yellow);
		g.fillOval(ballPosX, ballPosY, 20, 20);
		
		if(totalBricks <=0)
		{
			playing=false;
			ballDirX =0;
			ballDirY=0;
			g.setColor(Color.DARK_GRAY);
			g.setFont(new Font("serif",Font.BOLD, 25 ));
			g.drawString("You Won!" + score, 190, 300);
			
			g.setFont(new Font("serif",Font.BOLD, 25 ));
			g.drawString("Press Enter to restart " , 230, 350);
		}
		if(ballPosY > 570)
		{
			playing=false;
			ballDirX =0;
			ballDirY=0;
			g.setColor(Color.DARK_GRAY);
			g.setFont(new Font("serif",Font.BOLD, 25 ));
			g.drawString("Try again? Your score was:" + score, 190, 300);
			
			g.setFont(new Font("serif",Font.BOLD, 25 ));
			g.drawString("Press Enter to restart " , 230, 350);
		}

		g.dispose();

	}

}
