package com.nbu.breaker.brick.java;

import javax.swing.JFrame;

/*MAIN CLASS */

public class App 
{
    public static void main( String[] args )
    {
    	Gameplay gp = new Gameplay();
      JFrame jfr = new JFrame();
      jfr.setBounds(10,10,700,600);
      jfr.setResizable(false);
      jfr.setVisible(true);
      jfr.setTitle("NBU Brick Breaker");
     
      
      jfr.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      jfr.add(gp);
    }
}
