package com.nbu.breaker.brick.java;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;

public class MapGenerator {
	public int width;
	public int height;
	public int map[][];

	public MapGenerator(int rows, int cols) {

		map = new int[rows][cols];
		for (int i = 0; i < map.length; i++) {
			for (int j = 0; j < map[0].length; j++) {
				map[i][j] = 1;
			}
		}
		width = 540 / cols;
		height = 150 / rows;
	}

	public void draw(Graphics2D graphic) {
		for (int i = 0; i < map.length; i++) {
			for (int j = 0; j < map[0].length; j++) {
				if (map[i][j] > 0) {
					graphic.setColor(Color.WHITE);
					graphic.fillRect(j * width + 80, i * height + 50, width, height);
					graphic.setStroke(new BasicStroke(3));
					graphic.setColor(Color.BLACK);
					graphic.drawRect(j * width + 80, i * height + 50, width, height);
				}
			}
		}
	}

	public void setBrickValue(int value, int row, int col) {
		map[row][col] = value;
	}

}
